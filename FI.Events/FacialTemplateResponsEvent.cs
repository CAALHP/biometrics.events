﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FI.Events
{
    public class FacialTemplateResponsEvent: Event
    {
        public string UserId { get; set; }
        public byte[] Template { get; set; }
    }
}
