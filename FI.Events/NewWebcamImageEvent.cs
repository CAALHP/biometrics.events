﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FI.Events
{
    public class NewWebcamImageEvent: Event
    {

        public Bitmap Image { get; set; } 
    }

}
