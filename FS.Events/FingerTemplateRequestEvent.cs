﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Events
{
    public class FingerTemplateRequestEvent: Event
    {
        public string UserId { get; set; }
    }
}
