﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Events
{
    public class FingerTemplateResponsEvent: Event
    {
        public string UserId { get; set; }
        public byte[] Template { get; set; }
    }
}
